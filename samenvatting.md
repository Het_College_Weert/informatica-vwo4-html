# Basis HTML

De basis van iedere website is deze HTML code:

```html
<!DOCTYPE html>
<html>

<head>
</head>

<body>
</body>

</html>
```
Sla die tekst op in een html bestand: een bestand dat eindigt op `.html`. Bijvoorbeeld `website.html`. Let op: NIET `website.html.txt`; dit bestand eindigt op `.txt` en is dus geen html bestand.

Vervolgens open je dat `.html` bestand twee keer: een keer in een tekst bewerker zoals Kladblok of Notepad++, en een keer in een browser, bijvoorbeeld Chrome. Als je rechtermuisknop doet op het `.html` bestand kun je bij in het menu kiezen met welk programma (een tekst bewerker of een browser) je het opent.

In deze HTML code zie je verschillende _elementen_,:
- `html`
- `head`
- `body`

Ieder element heeft twee _tags_, een _open tag_ en een _close tag_. De open tag is altijd `<element>`; de close tag altijd `</element>`
Bijvoorbeeld, de open tag van het element `head` is `<head>` en de close tag van het element `head` is `</head>`.

De open en close tags van element `head` zitten tussen de open en close tags van het element `html`. Daarom zeggen we dat `head` _genest_ is in `html`. Op die manier is ook `body` genest in `html`. Je zou die structuur ook zo kunnen zien:

```
     html
    /    \
head      body

```
of zo:

`html`
- `head`
- `body`

`<!DOCTYPE html>` is een speciale tag die geen close tag nodig heeft en helemaal aan het begin van het `.html` bestand moet staan. Daar hoef je verder niet over na te denken.

Om inhoud aan je website te geven voeg je elementen toe. Dit doe je bijna altijd in het `body` element. Maar een paar elementen moeten in `head`. In de beschrijving en voorbeelden van een element vind je of het in `body` of `head` moet.

```html
<!DOCTYPE html>
<html>

<head>
  <title>Mijn website</title>
</head>

<body>
  <h1>Welkom!</h1>
  <p>Welkom op <i>mijn</i> website.</p>
  <p>Hier kun je dingen lezen.</p>
</body>

</html>
```
Deze structuur van elementen zou je ook zo kunnen zien:
```
     html
    /    \
head      body
  |      /  | \
title  h1   p  p
            |
            i
```
Of zo:

`html`
- `head`
  - `title`
- `body`
  - `h1`
  - `p`
    - `i`
  - `p`


In (dus tussen de open en close tags) van elementen als `h1`, `p` en `i` zet je tekst en andere elementen. Bij sommige elementen kun je nog meer zeggen. Bijvoorbeeld bij een link (het `a` element; `a` staat voor anchor) wil je zeggen naar welke website de link moet gaan.
```html
Klik op <a src="www.college.nl">deze link</a>.
```
`src` is een _atribute_. Attributes van een element staan altijd in de open tag, en altijd in deze vorm: `naame="waarde"`.

# Bestanden

Sommige elementen laten een bestand zien op je website. Het `img` element laat een plaatje op je website zien. Welke bestand dat plaatje is geef je aan met het attribute `src`:
```html
<img src="plaatje.png"></img>
```
Let op: het bestand `plaatje.png` moet precies die naam hebben en op je computer in dezelfde folder staan als het `.html` bestand:
```
Documenten/website.html
Documenten/plaatje.png
```

# Styling met CSS

Bij ieder element kun je het `style` attribute plaatsen:
```html
<p style="color: red;">Dit is rode tekst</p>
<p style="font-size: 50px;">Deze tekst is enorm!</p>
```
De waarde van het style attribute is code in een andere taal dan HTML. Die andere taal heet _CSS_.

CSS die je in het style attribute van een element schrijft geldt alleen voor dat ene element. Je kan ook CSS schrijven die geld voor meerdere elementen tegelijkertijd. Dit doe je in de het `style` element dat je in `head` plaatst op je pagina:

```html
<!DOCTYPE html>
<html>

<head>
  <style>
    p {
      color: green;
      font-size: 50px;
    }
  </style>
</head>

<body>
  <p>Deze tekst is groen en enorm.</p>
  <p>Deze tekst is ook groen en enorm!</p>
</body>

</html>
```
In de CSS in het `style` element staat: alle `p` elementen hebben de volgende styling: `color: green;` en `font-size: 50px;`.

In de taal CSS zijn `color` en `font-size` _properties_. Een styling regel ziet er altijd hetzelfde uit: `property`, dan `:`, dan de waarde, eindig met `;`.

Bij ieder element kun je ook een `id` attribute zetten. Dit kun je gebruiken om CSS te schrijven voor een specifiek element.
```html
<!DOCTYPE html>
<html>

<head>
  <style>
    p {
      color: green;
      font-size: 50px;
    }

    #foo {
      color: red;
    }
  </style>
</head>

<body>
  <p>Deze tekst is groen en enorm.</p>
  <p>Deze tekst is ook groen en enorm!</p>
  <p id="foo">Deze tekst is ook enorm en niet groen maar rood.</p>
</body>

</html>
```
In de CSS binnen het `style` element beschrijf je het element met id `foo` met de code `#foo`.


# Termen die je met herkennen
- HTML
- element
- tag
- nesting, genest
- attribute
- CSS
- property

# Wat je moet kunnen
- Een kale website maken
  - .html bestand maken
  - openen in je browser
  - aanpassen in een tekstbewerker
  - opslaan
  - browser pagina verversen
- Plaatjes toevoegen
- Styling aanpassen van elementen