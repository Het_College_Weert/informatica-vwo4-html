Jullie gaan iets maken wat hier op lijkt:

![screenshot](./screenshot.png)

Je mag andere plaatjes en kleuren verzinnen - en er zelf nog meer moois bij verzinnen - maar doe tenminste dit:

- Strook zo breed als de pagina met een plaatje als achtergrond.
- Daaronder in het midden van de pagina een plaatje
  - het bestand is een vierkant plaatje, maar met CSS maak je dat rond.
  - Met CSS geef je ook een gekleurde rand aan het plaatje.
- Onder het plaatje een quote.
- Onder de quote en rechts van het midden de naam van wie de quote is.

De HTML elementen die je nodig hebt:
- `div`
- `img`

De CSS properties die je gaan helpen zijn:
- `margin`
- `border`
- `border-radius`
- `text-align`
- `font-size`
- `background-image: url("plaatje.png");`